/**
 * Will import the module and export the default.
 *
 * This is a very lightweight implementation of the extract-loader/css extractor.
 */

exports.pitch = async function (remaining) {
  const result = await this.importModule(
    this.resourcePath + '.webpack[javascript/auto]' + '!=!' + remaining,
  )

  return result.default || result
}
