const loaderUtils = require('loader-utils')
const Module = require('module')
const path = require('path')
const fs = require('fs')
const ejs = require('ejs')

const generateToc = require('../toc')

const parentModule = module

const defaultOptions = {
  // Wether or not we should execute the module
  exec: true,

  // The replacement string to inject the content into the layout
  contentPlaceholder: '{{CONTENT}}',

  // Path where to find layouts
  layoutPath: [],

  // Data to merge into top level data
  globals: {},

  // Should we generate TOC?
  toc: false,

  // What to use to extract the frontmatter
  getFrontMatter: (content) => {
    return content.attributes
  },

  // What to use to get the content
  getContent: (content) => {
    return content.html
  },

  // defaultLayout
  defaultLayout: null,
}

// Shamelessly copied from webpack/val-loader
function exec(code, loaderContext) {
  const { resource, context } = loaderContext

  const module = new Module(resource, parentModule)

  // eslint-disable-next-line no-underscore-dangle
  module.paths = Module._nodeModulePaths(context)
  module.filename = resource

  // eslint-disable-next-line no-underscore-dangle
  module._compile(code, resource)

  return module.exports
}

// Search for file in paths
function searchFile(paths, file) {
  if (!(paths instanceof Array)) {
    paths = [paths]
  }

  return require.resolve('./' + file, { paths: paths.concat([process.cwd()]) })
}

function wrapWithLayout(body, frontMatter, options, loaderContext) {
  const layoutFile =
    (frontMatter.hasOwnProperty('layout') && frontMatter.layout) ||
    options.defaultLayout
  if (!layoutFile) {
    return body
  }

  const layoutPath = searchFile(options.layoutPath, layoutFile)
  if (!fs.existsSync(layoutPath)) {
    throw `No such layout: ${layoutPath}`
  }

  loaderContext.addDependency(layoutPath)

  const layoutSource = fs.readFileSync(layoutPath).toString('utf8')
  return layoutSource.replace(options.contentPlaceholder, body)
}

/**
 * This loader does 3 things:
 * - Optionally execute the source
 * - getFrontMatter and getContent to fetch the actual content and metadata
 * - load a layout file if it exists (uses frontmatter.layout or options.defaultLayout if not defined)
 * - generate a TOC of the content only (based on <hX> tags) if options.toc=true
 * - wrap the content in the loaded layout by string replace
 * - execute the content + layout as one .ejs template with:
 *   - data.frontMatter
 *   - data.toc (if generated)
 */
module.exports = function (source, map, meta) {
  const configOptions = loaderUtils.getOptions(this)
  const options = {
    ...defaultOptions,
    ...configOptions,
  }

  let content = source
  if (options.exec) {
    try {
      content = exec(content, this)
    } catch (error) {
      throw new Error(`Unable to execute "${this.resource}": ${error}`)
    }
  }

  const frontMatter = options.getFrontMatter(content)
  const body = options.getContent(content)
  const wrappedBody = wrapWithLayout(body, frontMatter, options, this)

  // Mark includes as dependencies
  ejs.fileLoader = (filePath) => {
    this.addDependency(filePath)
    return fs.readFileSync(filePath)
  }

  const template = ejs.compile(wrappedBody, {
    filename: this.resourcePath,
    localsName: 'data',
    includer: (originalPath, parsedPath) => {
      // Support for loading templates from node_modules
      if (originalPath.startsWith('~')) {
        let cleanPath = originalPath.replace(/^~/, '')
        cleanPath = cleanPath.endsWith('.ejs') ? cleanPath : cleanPath + '.ejs'

        // `paths` argument provided to ensure package lookup is made
        // within context of consuming app rather than dependency.
        parsedPath = require.resolve(cleanPath, { paths: [process.cwd()] })
      }
      return { filename: parsedPath }
    },
    views: [this.context, this.rootContext],
  })

  const templateData = {
    ...options.globals,
    frontMatter,
  }

  if (options.toc) {
    templateData.toc = generateToc(body)
  }

  return template(templateData)
}
