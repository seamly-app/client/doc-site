/**
 * HTML URL Rewrite loader
 *
 * This is a (limited) drop-in replacement for the HTML loader it will:
 * - require and potentially inline all img[src] attributes
 * - will rewrite local URL's in a[href], link[rel=stylesheet][href] and script[src]
 *
 */

const posthtml = require('posthtml')
const {
  getOptions,
  isUrlRequest,
  urlToRequest,
  stringifyRequest,
} = require('loader-utils')

const requireKey = (nr) => `__HTML_URL_REWRITE_REQUIRE_${nr}__`
const replaceKey = (nr) => `__HTML_URL_REWRITE_REPLACE_${nr}__`
const publicPathKey = '__HTML_URL_REWRITE_WEBPACK_PUBLIC_PATH__'

const rewritePublicUrl = (url) => {
  // External URLS
  if (url.match(/^(http|https|ws|wss|mailto|tel):/)) {
    return url
  }

  // Absolute URLS
  if (url.match(/^\//)) {
    return `${publicPathKey}${url.replace(/^\//, '')}`
  }

  return url
}

const rewriteRequireUrl = (url, map, root) => {
  if (!isUrlRequest(url, root)) {
    return url
  }

  const nr = Object.keys(map).length

  map[nr] = urlToRequest(decodeURIComponent(url), root)

  return replaceKey(nr)
}

const publicUrlProcessor = () => (tree) => {
  tree.match(
    [
      { tag: 'a', attrs: { href: /.+/ } },
      { tag: 'link', attrs: { rel: 'stylesheet', href: /^[^~].*/ } },
      { tag: 'script', attrs: { src: /.+/ } },
    ],
    (node) => {
      switch (node.tag) {
        case 'script':
          node.attrs.src = rewritePublicUrl(node.attrs.src)
          break
        default:
          node.attrs.href = rewritePublicUrl(node.attrs.href)
      }

      return node
    },
  )

  return tree
}

const requireUrlProcessor =
  ({ urlMap: map, root }) =>
  (tree) => {
    tree.match(
      [
        { tag: 'img', attrs: { src: /.+/ } },
        { tag: 'link', attrs: { rel: 'stylesheet', href: /^~.+/ } },
      ],
      (node) => {
        switch (node.tag) {
          case 'img':
            node.attrs.src = rewriteRequireUrl(node.attrs.src, map, root)
            break
          default:
            node.attrs.href = rewriteRequireUrl(node.attrs.href, map, root)
        }

        return node
      },
    )
  }

module.exports = function (source, map, meta) {
  const configOptions = getOptions(this)
  const options = {
    root: this.rootContext,
    ...configOptions,
  }

  let urlMap = {}

  const result = posthtml()
    .use(publicUrlProcessor())
    .use(requireUrlProcessor({ urlMap, root: options.root }))
    .process(source, { sync: true }).html

  const code = JSON.stringify(result)
    // Invalid in JavaScript but valid HTML
    .replace(/[\u2028\u2029]/g, (str) =>
      str === '\u2029' ? '\\u2029' : '\\u2028',
    )
    .replace(new RegExp(publicPathKey, 'g'), `" + __webpack_public_path__ + "`)
    .replace(/__HTML_URL_REWRITE_REPLACE_\d+__/g, (str) => `" + ${str} + "`)

  const requireCode = Object.keys(urlMap)
    .map((nr) => {
      escapedPath = stringifyRequest(this, urlMap[nr])
      return `import ${requireKey(nr)} from ${escapedPath};`
    })
    .join('\n')

  const replaceCode = Object.keys(urlMap)
    .map((nr) => {
      return `var ${replaceKey(nr)} = ${requireKey(nr)} && ${requireKey(
        nr,
      )}.__esModule ? ${requireKey(nr)}.default : ${requireKey(nr)};`
    })
    .join('\n')

  const all = `
    ${requireCode}
    ${replaceCode}
    var code = ${code};
    export default code;
  `

  return all
}
