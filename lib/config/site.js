const path = require('path')
const glob = require('glob')
const fs = require('fs')
const FMLMode = require('frontmatter-markdown-loader/mode')
const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const markdownItContainer = require('markdown-it-container')
const { ModuleFilenameHelpers } = require('webpack')
const postcssPresetEnv = require('postcss-preset-env')
const cssnano = require('cssnano')

const packageRoot = path.resolve(__dirname, '..', '..')

// Find files up in the tree
const findInTree = (dir, fileName) => {
  let parts = dir.split(path.sep)

  let packagePath
  let exists = false

  while (parts.length > 0 && !exists) {
    packagePath = [...parts, fileName].join(path.sep)
    exists = fs.existsSync(packagePath)
    parts.pop()
  }

  return exists && packagePath
}

/**
 * Loaders to rewrite URL's and emit the actual HTML files.
 *
 * @param {String} param0.siteRoot The root of the site
 * @param {String, Function} param0.outputName The name parameter passed to name for `file-loaderz
 */
const htmlExtractLoaders = ({ siteRoot, outputName }) => {
  return [
    require.resolve('../loaders/import-loader'),
    {
      loader: require.resolve('../loaders/html-url-rewrite-loader'),
      options: {
        root: siteRoot,
      },
    },
  ]
}

const ejsLoaders = ({
  siteRoot,
  templateGlobals,
  outputName,
  layoutLoaderOptions,
  markdownLoaderOptions,
}) => {
  return [
    ...htmlExtractLoaders({ siteRoot, outputName }),
    {
      loader: require.resolve('../loaders/ejs-layout-loader'),
      options: {
        layoutPath: [
          path.join(siteRoot, '_layouts'),
          path.resolve(packageRoot, 'templates', 'layouts'),
        ],
        globals: getGlobals({ siteRoot, templateGlobals }),
        getContent: (content) => {
          return content.body
        },
        ...layoutLoaderOptions,
      },
    },
    {
      loader: require.resolve('frontmatter-markdown-loader'),
      options: {
        mode: [FMLMode.BODY],
        ...markdownLoaderOptions,
      },
    },
  ]
}

const markdownLoaders = (options) => {
  return ejsLoaders({
    ...options,
    layoutLoaderOptions: {
      toc: true,
      defaultLayout: 'doc.ejs',
      getContent: (content) => {
        return content.html
      },
      ...options.layoutLoaderOptions,
    },
    markdownLoaderOptions: {
      markdownIt: markdownIt({
        html: true,
      })
        .use(markdownItAnchor)
        .use(markdownItContainer, 'box--important'),
      mode: [FMLMode.HTML],
      ...options.markdownLoaderOptions,
    },
  })
}

const BROWSERS = 'last 1 version, > 1%, not dead'

const assetScssLoaders = (options) => {
  return [
    require.resolve('../loaders/import-loader'),
    {
      loader: require.resolve('css-loader'),
      options: {
        import: false,
        sourceMap: false,
        modules: false,
        url: true,
        exportType: 'string',
      },
    },
    ...scssLoaders(options),
  ]
}

const styleScssLoaders = (options) => {
  return [
    'style-loader',
    {
      loader: require.resolve('css-loader'),
      options: {
        import: false,
        sourceMap: false,
        modules: false,
        url: true,
        exportType: 'array',
      },
    },
    ...scssLoaders(options),
  ]
}

const scssLoaders = (options) => {
  options = {
    browsers: BROWSERS,
    ...options,
  }
  return [
    {
      loader: require.resolve('postcss-loader'),
      options: {
        postcssOptions: {
          plugins: [
            postcssPresetEnv({
              browsers: options.browsers,
            }),
            cssnano(),
          ],
        },
        sourceMap: process.env.NODE_ENV !== 'production',
      },
    },
    {
      loader: require.resolve('sass-loader'),
      options: {
        sassOptions: {
          includePaths: [],
          precision: 10,
        },
      },
    },
  ]
}

/**
 * Make all paths relative to include paths and
 * transform these extensions
 * - .md -> .html
 * - .ejs -> .html
 * - .scss -> .css
 *
 * @param {string} siteRoot
 */
const pathResolver = (include) => (resource, resourceQuery) => {
  let sanitizedPath = findRelativePath(include, resource.module.resource)
  const ext = path.extname(resource.module.resource)

  if (!sanitizedPath) {
    console.warn(`Using default asset path for ${resource.module.resource}`)
    sanitizedPath = `assets/[name]-[hash]${ext}`
  }

  if (['.md', '.ejs'].includes(ext)) {
    sanitizedPath = sanitizedPath.replace(new RegExp(ext + '$'), '.html')
  }
  if (['.scss'].includes(ext)) {
    sanitizedPath = sanitizedPath.replace(new RegExp(ext + '$'), '.css')
  }
  return sanitizedPath
}

const findRelativePath = (paths, resourcePath) => {
  return paths
    .map((root) => path.relative(root, resourcePath))
    .find((sanitizedPath) => !sanitizedPath.startsWith('..'))
}

const getEntries = (siteRoot, { extensions, include, exclude }) => {
  const siteEntries = {}
  glob
    .sync(path.join(siteRoot, `**/*.{${extensions.join(',')}}`))
    .forEach((file) => {
      // Hack to fix output on windows machines
      if (path.sep === '\\') {
        file = file.replace(/\//g, '\\')
      }

      const base = path.basename(file)
      const url = path.relative(siteRoot, file)

      if (base.startsWith('_') || url.startsWith('_')) {
        return
      }

      if (!ModuleFilenameHelpers.matchObject({ include, exclude }, file)) {
        return
      }

      siteEntries[url] = file
    })

  return siteEntries
}

const getGlobals = ({ siteRoot, templateGlobals }) => {
  // Require package.json and add some keys to global.
  const pkgFile = findInTree(siteRoot, 'package.json')
  const siteConfigFile = findInTree(siteRoot, 'site.conf.js')

  const globals = {
    ...templateGlobals,
    pkg: {},
    site: {},
  }
  if (pkgFile) {
    const pkg = require(pkgFile)
    globals.pkg = {
      version: pkg.version,
      name: pkg.name,
      description: pkg.description,
    }
  }

  if (siteConfigFile) {
    const siteConfig = require(siteConfigFile)
    globals.site = siteConfig
  }

  return globals
}

module.exports = {
  htmlExtractLoaders,
  ejsLoaders,
  markdownLoaders,
  assetScssLoaders,
  styleScssLoaders,
  pathResolver,
  addSiteConfig: function (
    config,
    {
      siteRoot,
      templateGlobals,
      outputName: userOutputName,
      exclude: userExclude,
      include: userInclude,
    },
  ) {
    const exclude = userExclude || []
    const include = userInclude || [siteRoot]

    const outputName = userOutputName || pathResolver(include)

    const siteEntries = getEntries(siteRoot, {
      extensions: ['md', 'html', 'ejs'],
      include,
      exclude,
    })

    const siteRules = [
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        include,
        exclude,
        type: 'asset/resource',
        generator: {
          filename: outputName,
        },
      },
      // Only handle SCSS files for the package
      {
        test: /\.scss$/,
        include: [packageRoot],
        exclude,
        type: 'asset/resource',
        generator: {
          filename: outputName,
        },
        use: assetScssLoaders(),
      },
      {
        test: /\.(html|ejs)$/,
        include,
        exclude,
        type: 'asset/resource',
        generator: {
          filename: outputName,
        },
        use: ejsLoaders({ siteRoot, templateGlobals, outputName }),
      },
      {
        test: /\.md$/,
        include,
        exclude,
        type: 'asset/resource',
        generator: {
          filename: outputName,
        },
        use: markdownLoaders({ siteRoot, templateGlobals, outputName }),
      },
    ]

    const newConfig = {
      ...config,
      entry: {
        ...config.entry,
        ...siteEntries,
      },
      module: {
        ...config.module,
        rules: [...siteRules, ...config.module.rules],
      },
    }

    // If the default name is not set we set it here.
    if (!newConfig?.output?.assetModuleFilename) {
      newConfig['output'] = {
        ...newConfig.output,
        assetModuleFilename: outputName,
      }
    }

    return newConfig
  },
}
