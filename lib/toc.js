/**
 * Generate TOC tree based on <Hx id=...></Hx> tags
 *
 * RegEx based so YMMV works best with markdown generated content
 */
module.exports = function (source) {
  var tree = {
    title: 'TOC',
    id: null,
    level: 0,
    children: [],
  }

  var stack = [tree]
  var previous = null
  var currentLevel = 1

  source.replace(
    /<h(\d)(.*id="(.+?)")?>(.+?)<\/h\d>/gi,
    (_match, level, _idstr, id, title) => {
      level = level * 1

      var child = {
        title: title,
        level: level,
        id: id,
        children: [],
      }

      if (level > currentLevel) {
        stack.push(previous)
      } else if (level < currentLevel) {
        stack = stack.slice(0, level - currentLevel)
      }

      currentLevel = level
      previous = child

      stack[stack.length - 1].children.push(child)
    },
  )

  return tree
}
