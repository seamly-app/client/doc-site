# Seamly Doc Site Changelog

## [Unreleased]

## 4.0.0 (5 September 2024)

### Changed

- Updated `css-loader` to `7.1.x`
- Updated `cssnano` to `7.0.x`
- Updated `glob` to `11.0.x`
- Updated `markdown-it-anchor` to `9.1.x`
- Updated `postcss-preset-env` to `10.0.x`
- Updated `sass-loader` to `16.0.x`
- Updated `style-loader` to `4.0.x`

## 3.0.4 (5 September 2024)

### Changed

- Updated dependencies (minor and patch updates)
- Updated NodeJS version used to v20

## 3.0.3 (3 February 2024)

### Changed

- Minor dependency updates.

## 3.0.2 (2 November 2023)

### Changed

- Minor dependency updates.

## 3.0.1 (6 October 2023)

### Changed

- Minor dependency updates.

## 3.0.0 (7 August 2023)

### Changed

- Updated `cssnano` to `6.0.x`
- Updated `ejs` to `3.1.x`
- Updated `glob` to `10.3.x`
- Updated `postcss` to `10.3.x`
- Updated `prettier` to `3.0.x`
- Updated all webpack loaders

## 2.0.0 (9 August 2022)

### Changed

- Resolve all paths relative to the include directories
- The `scssLoader` export has been split into:
  - `assetScssLoader` for use in asset scenarios where the output is written to disk. You shouldn't need any other loaders only setup `type: 'asset/resource'` and `generator.filename`.
  - `styleScssLoader` for use in inline scenarios where the css will be injected by the `style-loader`.
- Remove the `extract-loader` and add a very simplistic `import-loader` that imports the previous stage and emits the default export.
- Use webpack asset modules to write files. This means if you have any custom files you generate you need to add `type: 'asset/resource` and a filename generator. Essentially replace code like this:

  ```
  {
    test: "xxx",
    use: site.markdownLoaders({
      outputName: 'xxx.html'
      // ....
    })
  }
  ```

  into

  ```
  {
    test: "xxxx",
    type: 'asset/resource',
    generator: {
      filename: 'xxx.html'
    },
    use: site.markdownLoaders({
      // ....
    })
  }
  ```

## 1.0.0 (19 January 2021)

### Changed

- Fixed the resolution of standard webpack-loaders through `resolveLoader.alias`

### Breaking Changes

- Upgrade to Webpack 5. This upgrade makes it incompatible with the previous major version.

## 0.3.4 (25 November 2020)

### Fixes

- Fix scrolling of navigation

## 0.3.3 (10 November 2020)

## Changed

- Change license to MIT

## 0.3.2 (4 November 2020)

## Changed

- Upgrade `webpack` and `file-loader` dependencies.

## 0.3.1 (14 October 2020)

### Added

- Add engine block to package.json

### Fixes

- Make doc-site work in Node 12+

## 0.3.0 (4 September 2020)

### Added

- Add support for deeper navigation nesting

## 0.2.1 (14 July 2020)

### Fixes

- Actually add contents to `demo` layout

## 0.2.0 (14 July 2020)

### Added

- Add `demo` layout

### Fixes

- Fix display of `siteTitle`
- Sanitize `default` layout

## 0.1.1 (14 July 2020)

### Added

- Add markdown-it-container for `box--important` so we can do `::: box--important` in MD

## 0.1.0 (14 July 2020)

### Added

- Support for mobile and IE11

### Changed

- Pages now have page-variant classes
- Structure of pages has changed

## 0.0.2 (3 July 2020)

### Added

- Add support for html in markdown

### Fixed

- Fix missing `glob` dependency
